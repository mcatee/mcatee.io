---
layout: page
permalink: "/projects/craft"
title: Craft
---

### projects

#### Paper 

#### Fabric & Vinyls 

- [Vinyl Patches](#)
- [Shirts](#)


### machines

- Silhouette  Came
- Ender 5 Pro 
- Heat Press 
- Stack Cutter
- Badge Press

#### wishlist

- Badge Printer 
- Sizer Juliet
- Drill Press
