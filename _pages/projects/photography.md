---
layout: page
title: Photography
permalink: /projects/photography
---


### Cameras
- [Fujifilm Instax Evo](https://www.instax.com.au/cameras/mini-evo/)
- Polaroid SX70 (_non-functional, some sort of roller issue_) ([Info](https://en.wikipedia.org/wiki/Polaroid_SX-70), [Examples](https://www.lomography.com/cameras/3315377-polaroid-sx-70/photos))
- [Lomography Sprocket Rocket](https://microsites.lomography.com/sprocketrocket/) ([Examples](https://www.lomography.com/cameras/3328515-lomography-sprocket-rocket/photos))
- [Kodak M38](https://www.kodak.com/en/consumer/product/cameras/film/m38) ([Examples](https://www.lomography.com/cameras/3362806-kodak-m38/photos))

### Wishlists

#### Cameras
- [Fujifilm Instax Wide](https://instax.com/wide/en/) (_Any model!_)
- [Fujifilm Instax SQ6](https://www.instax.com.au/cameras/square-sq6/) ([Examples](https://www.lomography.com/cameras/3358890-fuji-instax-square-sq6/photos))
- [Fujifilm Instax Square Link](https://instax.com/square_link/en/)
- Konica Pop! ([Examples](https://www.lomography.com/cameras/3324851-konica-pop/photos), [Info](http://camera-wiki.org/wiki/Konica_pop))
- GoPro Hero 9+ (_All are welcome, but the newer models are naturally more desirable!_)
- Polaroid SX70 (_a function one!_)

#### Film (_35mm_)
- StreetPan 400
- SantaColor 100
- RETO Amber D100/D400
- Kodak Portra 160
- Kodak Pro Image 100
- Dubblefilm Daily Colour 400
- Flic Elektra 100

_Frankly, film is so under demand at the moment that **any** film is really on my wishlist 😅_
