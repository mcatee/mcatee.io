---
layout: page
permalink: "/about"
title: About
---

- [GitLab: `@mcatee`](https://gitlab.com/mcatee) (primary)
  - [GitHub: `@mcat_ee`](https://github.com/mcat_ee)
- [Twitter: `@mcat_ee`](https://twitter.com/mcat_ee)
- `tom+website {at_symbol} mcatee {dot} io`


This site makes use of the following software & libraries:
- [Jekyll](https://jekyllrb.com/)
- [jekyll-anchor-headings](https://github.com/allejo/jekyll-anchor-headings)
- [Breadcrumbs](https://jekyllcodex.org/without-plugin/breadcrumbs/)